import "./App.css";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useState } from "react";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth } from "./firebase-config";
import { useFormik } from "formik";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
// import { Ref } from "react-hook-form";
import * as yup from "yup";
// import { useRef } from "react";

function App() {
  const [activeTab, setActiveTab] = useState<Boolean>(false);
  const [registerEmail, setRegisterEmail] = useState<string>("");
  const [registerPassword, setRegisterPassword] = useState<string>("");
  const [loginEmail, setLoginEmail] = useState<string>("");
  const [loginPassword, setLoginPassword] = useState<string>("");

  const [user, setUser] = useState<any>({});

  // const ref = useRef();

  const schema = yup.object().shape({
    loginEmail: yup
      .string()
      .email("Email is invalid")
      .required("Email is required"),
    loginPassword: yup.string().min(4).max(15).required("Password is required"),
    registerEmail: yup
      .string()
      .email("Email is invalid")
      .required("Email is required"),
    registerPassword: yup
      .string()
      .min(4)
      .max(15)
      .required("Password is required"),
  });

  const { register, handleSubmit, formState } = useForm<any>({
    resolver: yupResolver(schema),
  });
  const { errors } = formState;

  onAuthStateChanged(auth, (currentUser: any) => {
    setUser(currentUser);
  });

  const registerone = async (e: React.FormEvent) => {
    e.preventDefault();
    console.log("hello there");

    try {
      const userone = await createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPassword
      );
      console.log(userone);
    } catch (error: any) {
      console.log(error.message);
    }
    setRegisterEmail("");
    setRegisterPassword("");
  };

  const login = async (e: any) => {
    e.preventDefault();
    console.log("hello there");

    try {
      const loginuser = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
      console.log(loginuser);
    } catch (error: any) {
      console.log(error.message);
    }
    setLoginEmail("");
    setLoginPassword("");
  };

  const logout = async () => {
    await signOut(auth);
  };

  const handleClick = (e: React.FormEvent) => {
    e.preventDefault();
    setActiveTab(true);
  };

  const handleClick2 = (e: React.FormEvent) => {
    e.preventDefault();
    setActiveTab(false);
  };

  return (
    <div className="App">
      <div className="form">
        {!activeTab && (
          <form onSubmit={handleSubmit(login)}>
            <h1>Welcome back! </h1>
            <div className="inputs">
              <TextField
                id="outlined-basic"
                className="mt"
                label="Email"
                variant="outlined"
                type="email"
                value={loginEmail}
                {...register("loginEmail")}
                // ref={loginEmail}
                name="loginEmail"
                onChange={(e) => setLoginEmail(e.target.value)}
              />
              <p>{errors.loginEmail?.message}</p>
              <TextField
                id="outlined-basic"
                className="mt"
                label="Password"
                variant="outlined"
                type="password"
                value={loginPassword}
                {...register("loginPassword")}
                name="loginPassword"
                onChange={(e) => setLoginPassword(e.target.value)}
              />
              <p>{errors.loginPassword?.message}</p>
            </div>
            <Button type="submit" className="mt" variant="contained">
              Login
            </Button>
            {/* <h5>Or</h5> */}
            <h5>
              New here?{" "}
              <a onClick={handleClick} href="">
                Sign up
              </a>
            </h5>

            {/* <Button type="submit" className="mt" variant="contained">
            Sign up
          </Button> */}
          </form>
        )}

        {activeTab && (
          <form onSubmit={handleSubmit(registerone)}>
            <h1>Let's get started!</h1>
            <div className="inputs">
              <TextField
                id="outlined-basic"
                className="mt"
                label="Email"
                variant="outlined"
                value={registerEmail}
                {...register("registerEmail")}
                name="registerEmail"
                onChange={(e) => {
                  setRegisterEmail(e.target.value);
                }}
              />
              <p>{errors.registerEmail?.message}</p>
              <TextField
                id="outlined-basic"
                className="mt"
                label="Password"
                variant="outlined"
                type="password"
                value={registerPassword}
                {...register("registerPassword")}
                name="registerPassword"
                onChange={(e) => {
                  setRegisterPassword(e.target.value);
                }}
              />
              <p>{errors.registerPassword?.message}</p>
            </div>

            <Button type="submit" className="mt" variant="contained">
              Sign up
            </Button>
            <h5>
              Already a member?{" "}
              <a onClick={handleClick2} href="">
                Login
              </a>
            </h5>
          </form>
        )}
        <h4>Signed in as {user?.email}</h4>
        <Button onClick={logout} type="submit" variant="contained">
          Sign out
        </Button>
      </div>
    </div>
  );
}

export default App;
