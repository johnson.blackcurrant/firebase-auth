// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyATklXy8zMglO5R_mbQBLnRCh8OhFOdPUU",
  authDomain: "react-auth-8ebf7.firebaseapp.com",
  projectId: "react-auth-8ebf7",
  storageBucket: "react-auth-8ebf7.appspot.com",
  messagingSenderId: "590356027453",
  appId: "1:590356027453:web:2ae7da8dcb353d26d3b254",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

export function signup(email, password) {
  return createUserWithEmailAndPassword(auth, email, password);
}
